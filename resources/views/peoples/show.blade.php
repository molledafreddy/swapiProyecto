<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Peoples</title>
	<link rel="stylesheet"  href="/css/app.css">
</head>
<body>
	<div class="panel-body">
		<div>
			<h1 align="center">Character</h1>
		</div>
                <div class="col-sm-12 col-md-6">
                    <ul class="panel-list">
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Name</b></p>
                                <p>{{ $people -> name }}</p>
                            </div> 
                            <div class="col-sm-6 col-md-12 col-lg-6">
                                <p><b>Height</b></p>
                                <p>{{ $people -> height }}</p>
                            </div>
                        </li>
                        <li class="row">
                            <div class="col-sm-6 col-md-12 col-lg-4">
                                <p><b>Mass</b></p>
                                <p>{{ $people -> mass }}</p>
                            </div>
                            <div class="col-sm-6 col-md-12 col-lg-4">
                                <p><b>Hair color</b></p>
                                <p>{{ $people -> hair_color }}</p>
                            </div>                            
                        </li>
                        
                    </ul>
                </div>
                <div class="col-sm-12 col-md-6">
                    <ul class="panel-list">
                    <li class="row">
                        <div  class="col-sm-6 col-md-12 col-lg-4">
                            <p><b>Eye color</b></p>
                            <p class="text-capitalize">{{ $people -> eye_color }}</p>
                        </div>
                        <div  class="col-sm-6 col-md-12 col-lg-4">
                            <p><b>birth_year</b></p>
                            <p class="text-capitalize">{{ $people -> birth_year }}</p>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-4">
                            <p><b>gender</b></p>
                            <p>{{ $people -> gender }}</p>
                        </div> 
                    </li>
                    </ul>
                </div>
            </div>		
	
	<div class="container">
		<h1 align="center">Films</h1>
		
		<table class="table table-hover">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Director</th>
		        <th>Release date</th>
		        
		      </tr>
		    </thead>
		    <tbody>
		    @foreach($films as $film)
		    	   <tr>
			        <td class="text-capitalize">{{ $film['title'] }}</td>
			        <td class="text-capitalize">{{$film['director']}}</td>
			        <td class="text-capitalize">{{$film['release_date']}}</td>
			      </tr>
			      
		     @endforeach
		    </tbody>
	  	</table>
	</div>
</body>
</html>