<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Peoples</title>
	<link rel="stylesheet"  href="/css/app.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('vendor/font-awesome/css/font-awesome.css') }}">
</head>
<body>
	<div class="container">
		<div align="center">
			<h1>Characters</h1>
		</div>
		<div class="col-md-12">
			<form method="post" action=" {{ route('peoples.filter' ) }} ">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="post">
                    <div class="col-md-12" align="center">
                    	<div class="col-md-9">
                			<span class="control-label">Species: </span>
                			<select id="specie" name="species" class="form-control">
                                <option value="all">All</option>
                                @foreach($species as $specie)
                                    <option value="{{ $specie }}">{{ $specie }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-meritop-primary btn-md" id="addButton">Buscar</button>
                        </div>                        
                    </div>                
            </form>
		</div>
		    
       <table class="table table-hover text-capitalize">
		    <thead>
		      <tr>
		        <th>Gender</th>
		        <th>Name</th>
		        <th>Height</th>
		        <th>Skin color</th>
		        <th>Eye color</th>
		        
		      </tr>
		    </thead>
		    <tbody>
			@php
                $i=1;
            @endphp
            @foreach($peoples as $people)
		    	   <tr>
		    	   	<td class="text-capitalize">
                    @if ($people->gender == 'male')
                        <i class="fa fa-venus" aria-hidden="true"></i>
                    @elseif ($people->gender == 'female')
                        <i class="fa fa-mars" aria-hidden="true"></i>
                    @else
                        <i class="fa fa-android" aria-hidden="true"></i>
                    @endif</td>
			        <td><a href="{{ route('peoples.show', $i) }}">{{ $people->name }}</a></td>
			        <td class="text-capitalize">{{$people->height}}</td>
			        <td class="text-capitalize">{{$people->skin_color}}</td>
			        <td class="text-capitalize">{{$people->eye_color}}</td>
			      </tr>
			    @php
                $i++;
                @endphp   
		     @endforeach
		    </tbody>
	  	</table>
	</div>
</body>
</html>