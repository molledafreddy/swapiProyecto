<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class PeopleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $base_url = "https://swapi.co/api/";
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://swapi.co/api/',
        ]);
    
        $species = [];
        $response = $client->request('GET', 'people');

        $peoples = json_decode($response->getBody()->getContents())->results;

            foreach ($peoples as $key => $value){
                
                $route = substr($value->species[0],strlen('https://swapi.co/api/'));
                    
                $response = $client->request('GET', $route);
                
                $specie = json_decode($response->getBody()->getContents());
                            
                if (!in_array($specie->name,$species)) {
                    $species[] = $specie->name;
                }           
            }
            
        return view('peoples.index')->with([
            'peoples' => $peoples,
            
            'species' => $species
        ]);

    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $iconv( , out_charset, str)d
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://swapi.co/api/',
        ]);

        $response = $client->request('GET', 'people/'.$id );

        $people = json_decode($response->getBody()->getContents());

        foreach ($people->films as $key => $value){
            
            $route = substr($value,strlen('https://swapi.co/api/'));
                        
            $response = $client->request('GET', $route);
            
            $film = json_decode($response->getBody()->getContents());
                        
                $films[$key] =[ 
                'title'         => $film->title,
                'director'      => $film->director,
                'release_date'  => $film->release_date
            ];
        }
        
        return view('peoples.show')->with([
            'people'    => $people,
            'films'     => $films
        ]);
        
    }

    public function filter(Request $request)
    {

        $client = new Client(['base_uri' => 'https://swapi.co/api/']);
        $species = [];
        $p = [];     
        $response = $client->request('GET', 'people');
        $peoples = json_decode($response->getBody()->getContents())->results;
        
        if ( strcmp($request->species,"all") !== 0 ) {
                
                foreach ($peoples as $value){
                    $route = substr($value->species[0],strlen('https://swapi.co/api/'));
                    $response = $client->request('GET', $route);
                    
                    $specie = json_decode($response->getBody()->getContents());
                    if (!in_array($specie->name,$species)) {
                        $species[] = $specie->name;
                    }

                    if ($specie->name == $request->species) {
                        $p[] = $value;
                        
                    }
            }
           
            
         }else {
            
            foreach ($peoples as $value){
                $route = substr($value->species[0],strlen('https://swapi.co/api/'));
                $response = $client->request('GET', $route);
                $specie = json_decode($response->getBody()->getContents());

                if (!in_array($specie->name,$species)) {
                    $species[] = $specie->name;
                }
            }            

         }

         if (sizeOf($p)) {
             $result=$p;
         }else{
            $result=$peoples;
         }

         return view('peoples.index')->with([
            'peoples' => $result,
            'species' => $species
        ]);
        

    }

   
}
